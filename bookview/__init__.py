#!/bin/python3
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SECRET_KEY'] = '148f9a522bdd1a4a14551ffe2789a95d'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///bookcase.db'

# Shuts up a warning that isn't needed
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'False'

db = SQLAlchemy(app)

from bookview import routes

# Create the database so it can be used later
db.create_all()
