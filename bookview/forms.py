#!/bin/python3
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Length

class CreateForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    author = StringField('Author', validators=[DataRequired()])
    publisher = StringField('Publisher', validators=[DataRequired()])
    isbn = StringField('ISBN', validators=[DataRequired()])
    year = StringField('Year', validators=[DataRequired(), Length(min = 2, max = 4)])
    create = SubmitField('Create Entry')
