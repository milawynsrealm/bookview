#!/bin/python3
from flask import flash, request, url_for, redirect, render_template
from bookview import app
from bookview.forms import CreateForm
from bookview.models import Book, db

@app.route('/')
@app.route('/home')
def hello():
    return render_template('home.html', title='Home', books=Book.query.all())

@app.route('/create', methods=['GET','POST'])
def create():
    form = CreateForm()
    if request.method == 'POST':
        title = request.form['title']
        author = request.form['author']
        publisher = request.form['publisher']
        isbn = request.form['isbn']
        year = request.form['year']

        books = Book(title=title,
                     author=author,
                     publisher=publisher,
                     isbn=isbn,
                     year=year)
        db.session.add(books)
        db.session.commit()

    return render_template('create.html', title='Create Entry', form=form)
