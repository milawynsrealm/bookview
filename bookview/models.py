#!/bin/python3
from bookview import db

class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), unique=True, nullable=False)
    author = db.Column(db.String(120), unique=True, nullable=False)
    publisher = db.Column(db.String(120), unique=True, nullable=False)
    isbn = db.Column(db.String(120), unique=True, nullable=False)
    year = db.Column(db.String(4), unique=True, nullable=False)

    def __repr__(self):
        return f"Book('{self.title}', '{self.author}', '{self.publisher}', '{self.isbn}', '{self.year}')"

    def __init__(self, title, author, publisher, isbn, year):
        self.title = title
        self.author = author
        self.publisher = publisher
        self.isbn = isbn
        self.year = year
