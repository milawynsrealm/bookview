#!/bin/python3
from bookview import app

# Main entry point when running from the command line
if __name__ == '__main__':
    app.run(debug=True)
